#!/usr/bin/perl
use v5.10;
use DBI;
use File::Slurp;
use Time::HiRes;
$| = 1;

my $w1_dev = '/sys/devices/w1_bus_master1';
my $therm_val_file = "$w1_dev/28-0517c208acff/w1_slave";
my $relay_dev_num = 2;
my $relay_dev = "/sys/class/gpio/gpio$relay_dev_num";
my $relay_val_file = "$relay_dev/value";

# init therm
system 'modprobe w1_gpio' unless -e $w1_dev;
unless(-e $therm_val_file) {
	system 'modprobe w1_therm';
	sleep 5;
	die 'Failed to find ds18b20 sensor!' unless -e $therm_val_file;
}
# init relay
unless(-e $relay_dev) {
	write_file '/sys/class/gpio/export', $relay_dev_num;
	die 'Failed to init relay!' unless -e $relay_dev;
	write_file "$relay_dev/direction", 'out';
}
write_file $relay_val_file, 0;

my $dbh = DBI->connect('dbi:SQLite:dbname=therm.db', '','');
$dbh->do('CREATE TABLE IF NOT EXISTS therm_log (time INTEGER, therm REAL)');
$dbh->do('CREATE TABLE IF NOT EXISTS relay_log (time INTEGER, stat INTEGER)');
my $therm_log = $dbh->prepare('INSERT INTO therm_log (time, therm) VALUES (?, ?)');
my $relay_log = $dbh->prepare('INSERT INTO relay_log (time, stat) VALUES (?, ?)');

sub read_therm {  # TODO: rewrite to slurp
	open my $fh, '<', $therm_val_file;
	<$fh>;
	my $therm = <$fh>;
	close $fh;
	chomp $therm;
	$therm =~ s/.*(\d{5})$/$1/;
	$therm =~ s/(?<=^..)/./;
	return $therm;
}

my $delay = 25*1000;
my $on_dur = 2*60*1000;
my $stat = 1;
#my $run = 0;
my $last_time = 0;
sub relayctl {
	my ($time, $therm) = @_;
#tmp
#	$run = 1 if $therm < 19.5;
#	return unless $run;

	if ($time >= $last_time + ($stat ? $on_dur + $delay : $on_dur*7 - $delay)) {
		$stat = !$stat;
#if ($stat && $on_dur > 15*1000) {
#	$on_dur -= 15*1000;
#}
		write_file $relay_val_file, ($stat?1:0);
#		say "status: " . ($stat?'on':'off');
		$relay_log->execute($stat?$time+$delay:$time, $stat?1:0);
		$last_time = $time;
	}
}

my $last_therm;
while() {
	my $therm = read_therm;
	my $time = int(Time::HiRes::time * 1000);
	relayctl($time, $therm);
	if ($therm != $last_therm) {
#		say "$time $therm";
		$therm_log->execute($time, $therm);
		$last_therm = $therm;
	}
}

