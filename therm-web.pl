#!/usr/bin/perl
use v5.10;
use DBI;
use HTTP::Daemon;
use Time::HiRes;

my $dbh = DBI->connect('dbi:SQLite:therm.db', '', '');
my $therm_log = $dbh->prepare('SELECT * FROM therm_log WHERE time BETWEEN ? AND ? ORDER BY time');
my $relay_log = $dbh->prepare('SELECT * FROM relay_log WHERE time BETWEEN ? AND ? ORDER BY time');
my $d = HTTP::Daemon->new(
	LocalPort => 80,
	ReusePort => 1
) || die;

sub therm {
	my ($from_time, $to_time) = @_;
	my $html = <<'END';
		{
			type: "stepLine",
			xValueFormatString: "YYYY.MM.DD HH:mm:ss ",
			dataPoints: [
END
		$therm_log->execute($from_time, $to_time);
		while (my $record = $therm_log->fetchrow_hashref()) {
			$html .= <<END;
				{ x: new Date($record->{time}), y: $record->{therm} },
END
		}
		$html .= <<'END';
			]
		},
END
	return $html;
}

sub relay {
	my ($from_time, $to_time) = @_;
	my $html = <<'END';
		{
			type: "stepArea",
			axisYIndex: 1,
			xValueFormatString: "YYYY.MM.DD HH:mm:ss ",
			dataPoints: [
END
		$relay_log->execute($from_time, $to_time);
		while (my $record = $relay_log->fetchrow_hashref()) {
			my $stat = $record->{stat}?1:0;
			$html .= <<END;
				{ x: new Date($record->{time}), y: $stat },
END
		}
		$html .= <<'END';
			]
		},
END
	return $html;
}

sub on_dur {
	my ($from_time, $to_time) = @_;
	my $html = <<'END';
		{
			type: "stepLine",
			axisYIndex: 2,
			xValueFormatString: "YYYY.MM.DD HH:mm:ss ",
			dataPoints: [
END
		$relay_log->execute($from_time, $to_time);
		my $start_time;
		while (my $record = $relay_log->fetchrow_hashref()) {
			if ($record->{stat}) {
				$start_time = $record->{time};
			} elsif ($start_time) {
				my $dur = ($record->{time} - $start_time)/1000;
				$html .= <<END;
				{ x: new Date($start_time), y: $dur },
END
			$start_time = 0;
			}
		}
		$html .= <<'END';
			]
		},
END
	return $html;
}

sub integral {
	my ($from_time, $to_time) = @_;
	my $html = <<'END';
		{
			type: "stepLine",
			showInLegend: true,
			name: "Average temperature",
			axisYIndex: 0,
			xValueFormatString: "YYYY.MM.DD HH:mm:ss ",
			dataPoints: [
END
		$therm_log->execute($from_time, $to_time);
		my $seq = 5;
		my (@log, $extr_therm, $extr_time, $prev_extr_time, $prev_therm);
		my $is_rising = 0;
		my $rises = 0;
		my $falls = 0;
		while (my $record = $therm_log->fetchrow_hashref()) {
			my $time = $record->{time};
			my $therm = $record->{therm};
			push @log, [$time, $therm];
			if ($is_rising) {
				if ($therm > $extr_therm) {
					$extr_therm = $therm;
					$rises = $falls = 0;
					goto nxt;
				}
				if ($therm < $prev_therm) {
					$falls++;
					if ($falls >= $seq && $falls > $rises) {  # switch
						$is_rising = 0;
					}
				} elsif ($therm > $prev_therm) {
					$rises++;
				}
			} else {
				if (!defined $extr_therm || $therm < $extr_therm) {
					$extr_therm = $therm;
					$extr_time = $time;
					$rises = $falls = 0;
					goto nxt;
				}
				if ($therm > $prev_therm) {
					$rises++;
					if ($rises >= $seq && $rises > $falls) {  # switch
						$is_rising = 1;
						my $integral = 0;  # calculate average temperature
						while () {
							my ($cur_time, $cur_therm) = @{$log[0]};
							my ($next_time, $next_therm) = @{$log[1]};
							$integral += $cur_therm * ($next_time - $cur_time);
							if ($cur_time == $extr_time) {
								if (defined $prev_extr_time) {
									my $average_therm = $integral / ($extr_time - $prev_extr_time);
									$html .= <<END;
				{ x: new Date($prev_extr_time), y: $average_therm },
END
								}
								last;
							} else {
								shift @log;
							}
						}
						$prev_extr_time = $extr_time;
					}
				} elsif ($therm < $prev_therm) {
					$falls++;
				}
			}
nxt:
			$prev_therm = $therm;
		}
		$html .= <<'END';
			]
		},
END
	return $html;
}

while (my $c = $d->accept()) {
	while (my $r = $c->get_request()) {
		my %q = $r->uri->query_form();
		my $from = $q{from};
		my $range = $q{range};
		my $time = int(Time::HiRes::time * 1000);
		my $from_time = ($q{from} or ($range ? $time - $range*60*60*1000 : 0));
		my $to_time = (($from and $range) ? $from_time + $range*60*60*1000 : $time);
		my $html = <<'END';
<!DOCTYPE html>
<link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgo=">
<style>
html {
	height: 100%;
}
body {
	margin: 0;
	height: 100%;
}
</style>
<div id="chartContainer"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
var chart = new CanvasJS.Chart("chartContainer", {
	exportEnabled: true,
	axisX:{
		valueFormatString: "MMM DD HH:mm"
	},
	axisY:[
		{ 
			includeZero: false,
			interval: 1,
			suffix: "°C",
		},
		{
			title: "",
			tickLength: 0,
			lineThickness: 0,
			margin: 0,
			valueFormatString: " ",
			viewportMaximum: 1
		},
		{
			title: "",
			tickLength: 0,
			lineThickness: 0,
			margin: 0,
			valueFormatString: " ",
		}
	],
	data: [
END
	$html .= therm $from_time, $to_time;
	$html .= relay $from_time, $to_time;
	$html .= on_dur $from_time, $to_time;
	$html .= integral $from_time, $to_time;
	$html .= <<'END';
	]
})
chart.render()
</script>
END
		my $res = HTTP::Response->new(200);
		$res->header('Content-type', 'text/html; charset=utf-8');
		$res->content($html);
		$c->send_response($res);
	}
	$c->close();
	undef($c);
}

